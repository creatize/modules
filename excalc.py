#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods, @daffmaybe
# scope: hikka_min 1.6.3
# requires: mpmath sympy

import ast
from math import cos, factorial, radians, sin, sqrt, tan

from hikkatl.types import Message
from sympy import subfactorial

from .. import loader, utils


def safe_eval(expr):
    try:
        node = ast.parse(expr, mode="eval")
        result = eval(compile(node, "<string>", mode="eval"))
        return result
    except:
        return None


class Calc(loader.Module):
    strings = {
        "name": "Excalc",
        "err_int": "Only integral values",
        "_cls_doc": " Calculator with many functions. Only .e is better",
        "err_txt": "<b>[cl]</b> Error in a case",
    }

    strings_ru = {
        "err_int": "Только целые значения",
        "_cls_doc": " Калькулятор со множеством функций. Лучше только .e",
        "err_txt": "<b>[cl]</b> Ошибка в примере",
    }

    strings_es = {
        "err_int": "Solo valores enteros",
        "_cls_doc": "Una calculadora con muchas funciones. Mejor solo .e",
        "err_txt": "<b>[cl]</b> Error en el ejemplo",
    }

    strings_de = {
        "err_int": "Nur ganzzahlige Werte",
        "_cls_doc": "Ein Taschenrechner mit vielen Funktionen. Besser nur .e",
        "err_txt": "<b>[cl]</b> Fehler im Beispiel",
    }

    @loader.command()
    async def cl(self, message: Message):
        """- ваш пример"""
        args = utils.get_args_raw(message)

        try:
            await utils.answer(message, f"{args} = <code>{safe_eval(args)}</code>")
        except:
            await utils.answer(message, self.strings["err_txt"])

    @loader.command()
    async def fct(self, message: Message):
        """- вычисляет факториал числа"""
        args = utils.get_args_raw(message)

        try:
            await utils.answer(
                message, f"{args}! = <code>{round(factorial(int(args)), 5)}</code>"
            )
        except:
            await utils.answer(message, self.strings["err_int"])

    @loader.command()
    async def sqrt(self, message: Message):
        """- вычисляет квадратный корень числа"""
        args = utils.get_args_raw(message)

        await utils.answer(
            message, f"√{args} = <code>{round(sqrt(float(args)), 5)}</code>"
        )

    @loader.command()
    async def sfct(self, message: Message):
        """- вычисляет субфакториал числа"""
        args = utils.get_args_raw(message)

        try:
            await utils.answer(
                message, f"!{args} = <code>{round(subfactorial(int(args)), 5)}</code>"
            )
        except:
            await utils.answer(message, self.strings["err_int"])

    @loader.command()
    async def cos(self, message: Message):
        """- вычисляет косинус числа"""
        args = utils.get_args_raw(message)

        await utils.answer(
            message, f"cos({args}) = <code>{round(cos(radians(float(args))), 5)}</code>"
        )

    @loader.command()
    async def sin(self, message: Message):
        """- вычисляет синус числа"""
        args = utils.get_args_raw(message)

        await utils.answer(
            message, f"sin({args}) = <code>{round(sin(radians(float(args))), 5)}</code>"
        )

    @loader.command()
    async def tan(self, message: Message):
        """- вычисляет тангенс числа"""
        args = utils.get_args_raw(message)

        await utils.answer(
            message, f"tg({args}) = <code>{round(tan(radians(float(args))), 5)}</code>"
        )
