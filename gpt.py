#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods
# scope: hikka_min 1.6.3
# prompts from jailbreakchat.com
# requires: openai mistune

import logging

import mistune
import openai
from hikkatl.types import Message

from .. import loader, utils  # type: ignore

logger = logging.getLogger(__name__)


@loader.tds
class GPT(loader.Module):
    """Module for interacting with GPT using the OpenAI API"""

    strings = {
        "name": "GPT",
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> GPT is generating"
            " response, please wait</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Your"
            " question to ChatGPT was:</b> <code>{args}</code>"
        ),
        "quest_k": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Your"
            " question to KelvinGPT was:</b> <code>{args}</code>"
        ),
        "quest_j": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Your"
            " question to FuckGPT was:</b> <code>{args}</code>"
        ),
        "quest_a": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Your"
            " question to AIM was:</b> <code>{args}</code>"
        ),
        "quest_n": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Your"
            " initial prompt:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> You didn't ask a"
            " question GPT</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> You didn't provide an"
            " api key for GPT. Enter it in</b> <code>.cfg GPT</code>"
        ),
        "token_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> You have entered an"
            " invalid API key</b>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Error on OpenAI"
            " servers. Try repeating your question</b>"
        ),
        "floodwait": (
            "<emoji document_id=6332573220868196043>🕓</emoji><b> You have reached the"
            " request per minute limit. Retry your request in 10-40 seconds</b>"
        ),
    }

    strings_ru = {
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> GPT генерирует ответ,"
            " подождите</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ваш"
            " вопрос к ChatGPT был:</b> <code>{args}</code>"
        ),
        "quest_k": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ваш"
            " вопрос к KelvinGPT был:</b> <code>{args}</code>"
        ),
        "quest_j": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ваш"
            " вопрос к FuckGPT был:</b> <code>{args}</code>"
        ),
        "quest_a": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ваш"
            " вопрос к AIM был:</b> <code>{args}</code>"
        ),
        "quest_n": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ваш"
            " изначальный промпт:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Вы не задали вопрос"
            " GPT</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Вы не указали api key"
            " для GPT. Введите его в</b> <code>.cfg GPT</code>"
        ),
        "token_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Вы указали неверный"
            " API ключ</b>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Ошибка на серверах"
            " OpenAI. Попробуйте повторить свой вопрос</b>"
        ),
        "floodwait": (
            "<emoji document_id=6332573220868196043>🕓</emoji><b> Вы достигли лимита"
            " запросов в минуту. Повторите свой запрос через 10-40 секунд</b>"
        ),
    }

    strings_es = {
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> GPT está generando"
            " una respuesta, por favor espere</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Su"
            " pregunta a ChatGPT fue:</b> <code>{args}</code>"
        ),
        "quest_k": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Su"
            " pregunta a KelvinGPT fue:</b> <code>{args}</code>"
        ),
        "quest_j": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Su"
            " pregunta a FuckGPT fue:</b> <code>{args}</code>"
        ),
        "quest_a": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Su"
            " pregunta a AIM fue:</b> <code>{args}</code>"
        ),
        "quest_n": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Su"
            " prompt original fue:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> No ha hecho una"
            " pregunta a GPT</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> No ha indicado la"
            " clave API para GPT. Ingrese una con </b><code>.cfg GPT</code>"
        ),
        "token_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Ha indicado una clave"
            " API incorrecta</b>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Error en los"
            " servidores de OpenAI. Intente su pregunta de nuevo</b>"
        ),
        "floodwait": (
            "<emoji document_id=6332573220868196043>🕓</emoji><b> Ha alcanzado el"
            " límite de solicitudes por minuto. Intente su pregunta de nuevo en 10-40"
            " segundos</b>"
        ),
    }

    strings_de = {
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> GPT generiert eine"
            " Antwort, bitte warten Sie</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ihre"
            " Frage an ChatGPT lautete:</b> <code>{args}</code>"
        ),
        "quest_k": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ihre"
            " Frage an KelvinGPT lautete:</b> <code>{args}</code>"
        ),
        "quest_j": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ihre"
            " Frage an FuckGPT lautete:</b> <code>{args}</code>"
        ),
        "quest_a": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ihre"
            " Frage an AIM lautete:</b> <code>{args}</code>"
        ),
        "quest_n": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ihr"
            " ursprüngliches Prompt lautete:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Sie haben keine Frage"
            " an GPT gestellt</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Sie haben keinen"
            " API-Schlüssel für GPT angegeben. Geben Sie ihn mit</b> <code>.cfg"
            " GPT</code><b>an</b>"
        ),
        "token_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Sie haben den"
            " falschen API-Schlüssel angegeben</b>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Fehler auf den"
            " OpenAI-Servern. Versuchen Sie, Ihre Frage erneut zu stellen</b>"
        ),
        "floodwait": (
            "<emoji document_id=6332573220868196043>🕓</emoji><b> Sie haben das"
            " Zugriffslimit pro Minute erreicht. Stellen Sie Ihre Anfrage in 10-40"
            " Sekunden erneut</b>"
        ),
    }

    def __init__(self):
        self.config = loader.ModuleConfig(
            loader.ConfigValue(
                "api_key",
                None,
                lambda: "Api key for GPT",
                validator=loader.validators.Hidden(),
            ),
            loader.ConfigValue(
                "version",
                "gpt-3.5-turbo-16k",
                lambda: "Version of GPT",
                validator=loader.validators.Choice(
                    ["gpt-3.5-turbo-16k", "gpt-3.5-turbo", "gpt-4", "gpt-4-32k"]
                ),
            ),
            loader.ConfigValue(
                "organization",
                None,
                lambda: "Organization ID, maybe can give access to gpt-4",
            ),
            loader.ConfigValue(
                "custom_prompt",
                "You are a helpful assistant.",
                lambda: "Custom prompt",
                validator=loader.validators.String(),
            ),
        )

    @loader.command(
        ru_doc="- ваш вопрос к ChatGPT",
        es_doc="- tu pregunta a ChatGPT",
        de_doc="- deine Frage an ChatGPT",
    )
    async def gpt(self, message: Message):
        """- question to ChatGPT"""
        args = utils.get_args_raw(message)
        try:
            if not args:
                args = (await message.get_reply_message()).message  # type: ignore
        except:
            return await utils.answer(message, self.strings["args_err"])
        if self.config["api_key"] is None:
            return await utils.answer(message, self.strings["conf_err"])

        message = await utils.answer(message, self.strings["wait"])
        openai.api_key = self.config["api_key"]
        if self.config["organization"]:
            openai.organization = self.config["organization"]

        try:
            completion = openai.ChatCompletion.create(
                model=self.config["version"],
                messages=[{"role": "user", "content": args}],
            )
        except openai.error.RateLimitError:  # type: ignore
            logger.info(args)
            return await utils.answer(message, self.strings["floodwait"])
        except openai.error.AuthenticationError:  # type: ignore
            logger.info(args)
            return await utils.answer(message, self.strings["token_err"])
        except Exception as e:
            logger.info(args)
            logger.error(str(e))
            return await utils.answer(message, self.strings["api_err"])

        response = mistune.html(completion.choices[0].message.content)  # type: ignore
        await utils.answer(
            message, self.strings["quest"].format(answer=response, args=args)
        )

    @loader.command(
        ru_doc="- ваш вопрос к GPT с промптом",
        es_doc="- tu pregunta a GPT con prompt",
        de_doc="- deine Frage an GPT mit Prompt",
    )
    async def pgpt(self, message: Message):
        """- question to GPT with prompt"""
        args = utils.get_args_raw(message)
        try:
            if not args:
                args = (await message.get_reply_message()).message  # type: ignore
        except:
            return await utils.answer(message, self.strings["args_err"])
        if self.config["api_key"] is None:
            return await utils.answer(message, self.strings["conf_err"])

        message = await utils.answer(message, self.strings["wait"])
        openai.api_key = self.config["api_key"]
        if self.config["organization"]:
            openai.organization = self.config["organization"]

        try:
            completion = openai.ChatCompletion.create(
                model=self.config["version"],
                messages=[
                    {"role": "system", "content": self.config["custom_prompt"]},
                    {"role": "user", "content": args},
                ],
            )
        except openai.error.RateLimitError:  # type: ignore
            logger.info(args)
            return await utils.answer(message, self.strings["floodwait"])
        except openai.error.AuthenticationError:  # type: ignore
            logger.info(args)
            return await utils.answer(message, self.strings["token_err"])
        except Exception as e:
            logger.info(args)
            logger.error(str(e))
            return await utils.answer(message, self.strings["api_err"])
