#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods
# scope: hikka_min 1.6.3

import aiohttp
from hikkatl.types import Message

from .. import loader, utils


async def get_lorem(para, length):
    async with aiohttp.ClientSession() as session:
        async with session.get(
            f"https://loripsum.net/api/{para}/{length}/plaintext"
        ) as resp:
            return str((await resp.text()).strip())


class LoremGen(loader.Module):
    """Lorem Ipsum Generator"""

    strings = {"name": "LoremGen"}

    @loader.command()
    async def lorem(self, m: Message):
        args = utils.get_args(m)
        if not args:
            para = 2
            length = "medium"
        else:
            para, length = args

        result = await get_lorem(para, length)

        await utils.answer(m, result)
