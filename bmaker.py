#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods
# scope: hikka_min 1.6.3

from hikkatl.types import Message

from .. import loader, utils  # type: ignore


@loader.tds
class ButtonMaker(loader.Module):
    """"""

    strings = {
        "name": "ButtonMaker",
        "err": "Incorrect args",
        "bot_err": "Your inline bot did not respond. Try restarting the userbot",
    }

    strings_ru = {
        "err": "Неверные аргументы",
        "bot_err": "Ваш инлайн-бот не ответил. Попробуйте перезагрузить юзербота",
    }

    strings_es = {
        "err": "Argumentos incorrectos",
        "bot_err": "Su bot en línea no respondió. Intente reiniciar el bot de usuario",
    }

    strings_de = {
        "err": "Ungültige Argumente",
        "bot_err": (
            "Ihr Inline-Bot hat nicht geantwortet. Versuchen Sie, den Benutzerbot neu"
            " zu starten"
        ),
    }

    @loader.command(
        ru_doc=(
            "Создает сообщение с кнопкой. Агрументы: текст сообщения, название кнопки,"
            " ссылка, разделенные <>"
        ),
        de_doc=(
            "Erstellt eine Nachricht mit einer Schaltfläche. Argumente: Text der"
            " Nachricht, Name der Schaltfläche, Link, getrennt durch <>"
        ),
        es_doc=(
            "Crea un mensaje con un botón. Argumentos: texto del mensaje, nombre del"
            " botón, enlace separado por <>"
        ),
    )
    async def inl(self, m: Message):
        """Creates a button. Args: str, str, link separated by <>"""
        args = utils.get_args_split_by(m, "<>")
        if len(args) != 3:
            await utils.answer(m, self.strings["err"])
            return
        if not args[2].startswith("https://") and not args[2].startswith("http://"):
            args[2] = "https://" + args[2]

        try:
            await self.inline.form(
                message=m,
                text=args[0],
                reply_markup=[[{"text": args[1], "url": args[2]}]],
            )
            await m.delete()  # type: ignore
        except:
            await utils.answer(m, self.string["bot_err"])
