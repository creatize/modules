#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods
# scope: hikka_min 1.6.3

import random

from hikkatl.types import Message

from .. import loader, utils


async def inline_search(client, query, inline_bot):
    try:
        result = await client.inline_query(inline_bot, query)
        if not result:
            return None
        return result
    except Exception:
        return None


class Searcher(loader.Module):
    """Search in some bots"""

    strings = {
        "name": "Searcher",
        "err": "<emoji document_id=5260342697075416641>❌</emoji> Not found",
    }

    strings_ru = {"err": "<emoji document_id=5260342697075416641>❌</emoji> Не найдено"}

    @loader.command()
    async def pic(self, m: Message):
        """<str> - search in @pic"""
        args = utils.get_args_raw(m)

        if await m.get_reply_message():
            r = await m.get_reply_message()
        else:
            r = m

        result = await inline_search(self._client, args, "pic")
        if not result:
            return await utils.answer(m, self.strings("err"))

        await result[random.randint(0, 20)].click(m.to_id, reply_to=r)
        await m.delete()

    @loader.command()
    async def vid(self, m: Message):
        """<str> - search in @vid"""
        args = utils.get_args_raw(m)

        if await m.get_reply_message():
            r = await m.get_reply_message()
        else:
            r = m

        result = await inline_search(self._client, args, "vid")
        if not result:
            return await utils.answer(m, self.strings("err"))

        await result[random.randint(0, 1)].click(m.to_id, reply_to=r)
        await m.delete()

    @loader.command()
    async def mod(self, m: Message):
        """<str> - search in @ftg2bot"""
        args = utils.get_args_raw(m)

        if await m.get_reply_message():
            r = await m.get_reply_message()
        else:
            r = m

        result = await inline_search(self._client, args, "ftg2bot")
        if not result:
            return await utils.answer(m, self.strings("err"))

        await result[0].click(m.to_id, reply_to=r)
        await m.delete()
