#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods
# scope: hikka_min 1.6.3
# requires: aioclaude-api mistune

import mistune
from aioclaude_api import AIOclient as Client
from hikkatl.types import Message

from .. import loader, utils  # type: ignore


@loader.tds
class ClaudeAPI(loader.Module):
    """Module for interaction with Claude right in telegram"""

    strings = {
        "name": "ClaudeAPI",
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> Claude is generating"
            " response, please wait</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Your"
            " question to Claude was:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> You didn't ask a"
            " question to Claude</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> You didn't provide an"
            " API-key for Claude. Enter it in</b> <code>.cfg ClaudeAPI</code>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Error on Claude"
            " servers. Try repeating your question</b>"
        ),
        "no_chat": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> You don't have a chat"
            " with Claude</b>"
        ),
        "chat_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Chat cleared"
            " successfully.</b>"
        ),
        "chats_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Chats cleared"
            " successfully.</b>"
        ),
    }

    strings_ru = {
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> Claude генерирует"
            " ответ, пожалуйста, подождите</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ваш"
            " вопрос для Claude был:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Вы не задали вопрос"
            " Claude</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Вы не предоставили"
            " API-ключ для Claude. Введите его в</b> <code>.cfg ClaudeAPI</code>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Ошибка на серверах"
            " Claude. Попробуйте повторить свой вопрос</b>"
        ),
        "no_chat": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> У вас нет чата с"
            " Claude</b>"
        ),
        "chat_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Чат успешно очищен."
            "</b>"
        ),
        "chats_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Чаты успешно очищены."
            " </b>"
        ),
    }

    strings_es = {
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> Claude está generando"
            " una respuesta, por favor espere</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Su"
            " pregunta para Claude fue:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> No ha hecho una"
            " pregunta a Claude</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> No ha proporcionado"
            " una clave API para Claude. Ingrésela con</b> <code>.cfg ClaudeAPI</code>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Error en los"
            " servidores de Claude. Intente hacer su pregunta de nuevo</b>"
        ),
        "no_chat": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> No tiene un chat con"
            " Claude</b>"
        ),
        "chat_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Chat borrado"
            " exitosamente.</b>"
        ),
        "chats_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Chats borrados"
            " exitosamente.</b>"
        ),
    }

    strings_de = {
        "wait": (
            "<emoji document_id=5443038326535759644>💬</emoji><b> Claude generiert"
            " eine Antwort, bitte warten Sie</b>"
        ),
        "quest": (
            "{answer}\n\n\n<emoji document_id=5452069934089641166>❓</emoji><b> Ihre"
            " Frage an Claude war:</b> <code>{args}</code>"
        ),
        "args_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Sie haben Claude"
            " keine Frage gestellt</b>"
        ),
        "conf_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Sie haben keinen"
            " API-Schlüssel für Claude angegeben. Geben Sie ihn ein mit</b> <code>.cfg"
            " ClaudeAPI</code>"
        ),
        "api_err": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Fehler auf Claudes"
            " Servern. Versuchen Sie es erneut</b>"
        ),
        "no_chat": (
            "<emoji document_id=5210952531676504517>❌</emoji><b> Sie haben keinen Chat"
            " mit Claude</b>"
        ),
        "chat_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Chat erfolgreich"
            " gelöscht.</b>"
        ),
        "chats_cleared": (
            "<emoji document_id=5206607081334906820>✔️</emoji><b> Chats erfolgreich"
            " gelöscht.</b>"
        ),
    }

    def __init__(self):
        self.config = loader.ModuleConfig(
            loader.ConfigValue(
                "api_key",
                None,
                lambda: (
                    "Api key for Claude. How to get:"
                    " https://github.com/KoushikNavuluri/Claude-API#usage"
                ),
                validator=loader.validators.Hidden(),
            )
        )

    async def client_ready(self, client, db):
        self.client = client
        self.db = db

    @loader.command(
        ru_doc="- ваш вопрос к Claude",
        es_doc="- tu pregunta para Claude",
        de_doc="- deine Frage an Claude",
    )
    async def cl(self, m: Message):
        """- your question to Claude"""
        args = utils.get_args_raw(m)
        try:
            if not args:
                args = (await m.get_reply_message()).message  # type: ignore
        except:
            return await utils.answer(m, self.strings["args_err"])
        if self.config["api_key"] is None:
            return await utils.answer(m, self.strings["conf_err"])

        m = await utils.answer(m, self.strings["wait"])
        claude = Client(self.config["api_key"])

        conv_id = self.db.get(self.name, "conv_id", None)
        if not conv_id:
            conv_id = (await claude.create_new_chat())["uuid"]
            self.db.set(self.name, "conv_id", conv_id)

        response = mistune.html(await claude.send_message(args, conv_id))

        await utils.answer(m, self.strings["quest"].format(answer=response, args=args))

    @loader.command(
        ru_doc="- ваш вопрос к Claude (без истории диалога)",
        es_doc="- tu pregunta para Claude (sin historial de diálogo)",
        de_doc="- deine Frage an Claude (ohne Dialogverlauf)",
    )
    async def c(self, m: Message):
        """- your question to Claude (without dialog history)"""
        args = utils.get_args_raw(m)
        try:
            if not args:
                args = (await m.get_reply_message()).message  # type: ignore
        except:
            return await utils.answer(m, self.strings["args_err"])
        if self.config["api_key"] is None:
            return await utils.answer(m, self.strings["conf_err"])

        m = await utils.answer(m, self.strings["wait"])
        claude = Client(self.config["api_key"])
        conv_id = (await claude.create_new_chat())["uuid"]

        response = mistune.html(await claude.send_message(args, conv_id))

        await claude.delete_conversation(conv_id)

        await utils.answer(m, self.strings["quest"].format(answer=response, args=args))

    @loader.command(
        ru_doc="- очистить чат с Claude",
        es_doc="- borrar chat con Claude",
        de_doc="- chat mit Claude löschen",
    )
    async def delchat(self, m: Message):
        """Clears the chat with Claude"""
        claude = Client(self.config["api_key"])
        conv_id = self.db.get(self.name, "conv_id", None)
        if not conv_id:
            return await utils.answer(m, self.strings["no_chat"])
        await claude.delete_conversation(conv_id)
        self.db.set(self.name, "conv_id", None)
        await utils.answer(m, self.strings["chat_cleared"])

    @loader.command(
        ru_doc="- очистить все чаты с Claude",
        es_doc="- borrar todos los chats con Claude",
        de_doc="- alle Chats mit Claude löschen",
    )
    async def delallchats(self, m: Message):
        """- deletes all chats with Claude"""
        claude = Client(self.config["api_key"])
        await claude.reset_all()
        await utils.answer(m, self.strings["chats_cleared"])
