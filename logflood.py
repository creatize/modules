#  ______     ______     ______     ______     ______   __     ______     ______
# /\  ___\   /\  == \   /\  ___\   /\  __ \   /\__  _\ /\ \   /\___  \   /\  ___\
# \ \ \____  \ \  __<   \ \  __\   \ \  __ \  \/_/\ \/ \ \ \  \/_/  /__  \ \  __\
#  \ \_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_\   /\_____\  \ \_____\
#   \/_____/   \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_/   \/_____/   \/_____/

# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.

# meta developer: @creaz_mods
# scope: hikka_min 1.6.3
# requires: logging

import logging

from hikkatl.types import Message

from .. import loader, utils


@loader.tds
class logflood(loader.Module):
    """Floods in logs."""

    strings = {
        "name": "logflood",
        "no_args": (
            "<emoji document_id=5260342697075416641>❌</emoji><b> You did not specify"
            " arguments"
        ),
        "done": "<emoji document_id=5784891605601225888>🔵</emoji> Successfully!",
        "wait": "<emoji document_id=5220118571706755791>🕒</emoji> Please wait...",
    }

    strings_ru = {
        "no_args": (
            "<emoji document_id=5260342697075416641>❌</emoji><b> Вы не указали"
            " аргументы"
        ),
        "wait": "<emoji document_id=5220118571706755791>🕒</emoji> Ожидайте...",
        "done": "<emoji document_id=5784891605601225888>🔵</emoji> Успешно!",
        "_cls_doc": "Флудит в логи.",
    }

    strings_es = {
        "no_args": (
            "<emoji document_id=5260342697075416641>❌</emoji><b> No has especificado"
            " argumentos"
        ),
        "wait": "<emoji document_id=5220118571706755791>🕒</emoji> Espera...",
        "done": "<emoji document_id=5784891605601225888>🔵</emoji> ¡Hecho!",
        "_cls_doc": "Inunda los registros.",
    }

    strings_de = {
        "no_args": (
            "<emoji document_id=5260342697075416641>❌</emoji><b> Sie haben keine"
            " Argumente angegeben"
        ),
        "wait": "<emoji document_id=5220118571706755791>🕒</emoji> Warten Sie...",
        "done": "<emoji document_id=5784891605601225888>🔵</emoji> Erfolgreich!",
        "_cls_doc": "Flutet die Logs.",
    }

    def __init__(self):
        self.config = loader.ModuleConfig(
            loader.ConfigValue(
                "lvl",
                "info",
                lambda: "logging level",
                validator=loader.validators.Choice(["info", "error", "warning"]),
            ),
        )

    @loader.command()
    async def lf(self, m: Message):
        """message >> count"""
        args = utils.get_args_split_by(m, ">>")

        if len(args) != 2:
            return await utils.answer(m, self.strings["no_args"])

        log_level = self.config["lvl"]
        log_func = {
            "info": logging.info,
            "error": logging.error,
            "warning": logging.warning,
        }.get(log_level)

        if log_func is None:
            return await utils.answer(m, self.strings["no_args"])

        await utils.answer(m, self.strings["wait"])
        for _ in range(int(args[1])):
            log_func(args[0])

        await utils.answer(m, self.strings["done"])
